/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.manita.reportproject;

import java.util.List;

/**
 *
 * @author user
 */
public class ArtistService {

    public List<ArtistReport> getTopTenArtistByTotalPrice() {
        ArtsitDao artistDao = new ArtsitDao();
        return artistDao.getArtistByTotalPrice(10);
    }

    public List<ArtistReport> getTopTenArtistByTotalPrice(String begin,String end) {
        ArtsitDao artistDao = new ArtsitDao();
        return artistDao.getArtistByTotalPrice(begin,end,10);
    }
}
